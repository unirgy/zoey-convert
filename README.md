# ZoeyConvert.php

This script was created to help developers convert existing Magento 1.x extensions to be submitted to Zoey.

It can be used as a standalone script from command line, or as an include in other PHP scripts for custom use.

At this moment it does not perform any validation for proper original file/folder structure or config files.

## Standalone Usage

    $ php ZoeyConvert.php <sourceDir> <targetDir> [--debug]
        
        <sourceDir>  Root directory containing source modules folders in format:
                     "<Namespace>_<Module>". Module folders and files structure
                     should be the same as it would be in Magento 1 installation
                     
        <targetDir>  Root directory where "vendor" folder lives. 
        
        --debug      Show debug log information

## Example

Given the following folder structure:

	zoey/ (current working directory)
	.	zoey-convert/
	.	.	ZoeyConvert.php
	.	orig-modules/
	.	.	Namespace_Module/
	.	.	.	app/...
	.	.	.	design/...
	.	.	.	skin/...
	.	zoey-core/
	.	.	app/...
	.	.	vendors/
	.	.	.	Namespace/
	.	.	.	.	Module/
	
This command will convert all original modules in `orig-modules` to `zoey-core/vendors/Namespace`:

	$ php zoey-convert/ZoeyConvert.php orig-modules zoey-core
	
## Implemented Features

The steps were taken from this URL: https://developers.zoey.com/docs/converting-your-magento-extension-to-zoey

* Create module registration file (`module_status`)
* Move layouts, templates, skin files, javascript files, emails and translations to proper locations within the module
* Update `etc/config.xml`
	* Add layout `module` tags
	* Add template, skin, js `folders` tags
	* Add emails and translations `in_module` tags
* Update `etc/system.xml`
	* Add `config_path` for settings fields that use `source_model` (may be removed later if not necessary anymore)
	
## Ideas for future features

* Add structure and configuration validation