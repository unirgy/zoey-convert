<?php

/**
* Zoey module conversion script
* 
* Can be used as a standalone CLI script, or included in another php file
* 
* @license MIT
* @author Boris Gurvich
* @copyright Unirgy LLC (R) 2017
*/
class ZoeyConvert
{
    protected $_config;
    
    public function __construct($config = [])
    {
        $this->_config = $config;
    }
    
    public function convertAllModulesInDir($rootDir = null)
    {
        if (null === $rootDir) {
            $rootDir = $this->_config['source_dir'];
        }
        $this->_log("Scanning {$rootDir} for modules...");
        $modDirs = @glob("{$rootDir}/*");
        if (!$modDirs) {
            $this->_log("No module dirs found in {$rootDir}", 'WARNING');
            return $this;
        }
        foreach ($modDirs as $modDir) {
            $folderName = basename($modDir);
            $this->_log("Found folder {$folderName}");
            $arr = explode('_', $folderName);
            if (sizeof($arr) !== 2) {
                $this->_log("Invalid module folder name (expecting Vendor_Module): {$folderName}");
                continue;
            }
            list($namespace, $modName) = $arr;
            $this->convertModule($modDir, $namespace, $modName);
        }
        return $this;
    }
    
    public function convertModule($sourceRootDir, $namespace, $module)
    {
        $nsMod = "{$namespace}_{$module}";
        $this->_log("Converting {$nsMod}...");
        
        $targetModDir = "{$this->_config['target_dir']}/vendor/{$namespace}/{$module}";
        if (file_exists($targetModDir)) {
            $this->_delTree($targetModDir);
        }
        if (!@mkdir($targetModDir, 0775, true)) {
            $this->_log("Target dir is not writable: {$this->_config['target_dir']}", 'ERROR');
            return $this;
        }
        
        $templateFolders = [];
        $skinFolders = [];
        $jsFolders = [];
        
        // COPY: module code
        // FROM: <web_root>/app/code/<pool>/<Namespace>/<Module>/
        // TO:   <web_root>/vendor/<Namespace>/<Module>/
        $sourceModGlob = "{$sourceRootDir}/app/code/*/{$namespace}/{$module}";
        $sourceModDirs = @glob($sourceModGlob);
        if (!$sourceModDirs) {
            $this->_log("No module code found in {$sourceModGlob}", 'ERROR');
            return $this;
        }
        $sourceModDir = $sourceModDirs[0];
        $this->_copyTree("{$sourceModDir}/*", $targetModDir);

        // COPY: module registration
        // FROM: <web_root>/app/etc/modules/<Namespace>_<Module>.xml
        // TO:   <web_root>/vendor/<Namespace>/<Module>/module_status.xml
        // UPDATE: module_status.xml
        // XML:   /modules/<Namespace>_<Module>/codePool = vendor
        $fileContents = file_get_contents("{$sourceRootDir}/app/etc/modules/{$nsMod}.xml");
        $fileContents = preg_replace('#<codePool>(core|community|local)</codePool>#i', '<codePool>vendor</codePool>', $fileContents);
        file_put_contents("{$targetModDir}/module_status.xml", $fileContents);
        
        // COPY: layout files
        // FROM: <web_root>/app/design/<area>/<package>/<theme>/layout/<folder>
        // TO:   <web_root>/vendor/<Namespace>/<Module>/design/<area>/<folder>
        // COPY: template files
        // FROM: <web_root>/app/design/<area>/<package>/<theme>/template/<folder>/
        // TO:   <web_root>/vendor/<Namespace>/<Module>/design/<area>/template/<folder>/
        $areaDirs = @glob("{$sourceRootDir}/app/design/*");
        if ($areaDirs) {
            foreach ($areaDirs as $areaDir) {
                $area = basename($areaDir);
                $this->_copyTree("{$areaDir}/*/*/layout/*", "{$targetModDir}/design/{$area}");
                $tplDirs = @glob("{$areaDir}/*/*/template/*");
                if ($tplDirs) {
                    foreach ($tplDirs as $d) {
                        $templateFolders[$area][basename($d)] = 1;
                        $this->_copyTree($d, "{$targetModDir}/design/{$area}/template");
                    }
                }
            }
        }
        
        // COPY: skin files
        // FROM: <web_root>/skin/<area>/<package>/<theme>/<folder>/
        // TO:   <web_root>/vendor/<Namespace>/<Module>/skin/<area>/<folder>/
        $areaDirs = @glob("{$sourceRootDir}/skin/*");
        if ($areaDirs) {
            foreach ($areaDirs as $areaDir) {
                $area = basename($areaDir);
                $skinDirs = @glob("{$areaDir}/*/*/*");
                if ($skinDirs) {
                    foreach ($skinDirs as $d) {
                        $skinFolders[$area][basename($d)] = 1;
                        $this->_copyTree($d, "{$targetModDir}/skin/{$area}");
                    }
                }
            }
        }
        
        // COPY: js files
        // FROM: <web_root>/js/<folder>/
        // TO:   <web_root>/vendor/<Namespace>/<Module>/js/<folder>/
        $jsDirs = @glob("{$sourceRootDir}/js/*");
        if ($jsDirs) {
            foreach ($jsDirs as $d) {
                $jsFolders[basename($d)] = 1;
                $this->_copyTree($d, "{$targetModDir}/js");
            }
        }
        
        // COPY: email files
        // FROM: <web_root>/app/locale/<locale>/template/email/<path>
        // TO:   <web_root>/vendor/<Namespace>/<Module>/locale/<locale>/template/email/<path>
        // COPY: translation files
        // FROM: <web_root>/app/locale/<locale>/<file>
        // TO:   <web_root>/vendor/<Namespace>/<Module>/locale/<locale>/<file>
        $this->_copyTree("{$sourceRootDir}/app/locale/*", "{$targetModDir}/locale");
        
        // UPDATE: etc/config.xml
        $configFile = "{$targetModDir}/etc/config.xml";
        $configXml = simplexml_load_file($configFile);
		// XML: /<area>/layout/updates/*/module = <Namespace>_<Module>
		$nodes = $configXml->xpath('*/layout/updates/*');
		if ($nodes) {
			foreach ($nodes as $n) {
				$n->module = $nsMod;
			}
		}
		
        // XML: /zoey_global/template/<Namespace>_<Module>/<area>/folders = folder1,folder2
        if ($templateFolders) {
            foreach ($templateFolders as $area => $folders) {
                $configXml->zoey_global->template->$nsMod->$area->folders = join(',', array_keys($folders));
            }
        }
        // XML: /zoey_global/skin/<Namespace>_<Module>/<area>/folders = folder1,folder2
        if ($skinFolders) {
            foreach ($skinFolders as $area => $folders) {
                $configXml->zoey_global->skin->$nsMod->$area->folders = join(',', array_keys($folders));
            }
        }
        // XML: /zoey_global/js/<Namespace>_<Module>/all/folders = folder
        if ($jsFolders) {
            $configXml->zoey_global->js->$nsMod->all->folders = join(',', array_keys($jsFolders));
        }
        // XML: /global/template/email/<email_name>/in_module = <Namespace>_<Module>
        $nodes = $configXml->xpath('global/template/email/*');
        if ($nodes) {
            foreach ($nodes as $n) {
                $n->in_module = $nsMod;
            }
        }
        // XML: /<area>/translate/modules/<Namespace>_<Module>/files/default[in_module=1] = <file>
        $nodes = $configXml->xpath("*/translate/modules/{$nsMod}/files/default");
        if ($nodes) {
            foreach ($nodes as $n) {
                $n['in_module'] = 1;
            }
        }
        
        file_put_contents($configFile, $configXml->asXML());
		
		// UPDATE: etc/system.xml
        $systemFile = "{$targetModDir}/etc/system.xml";
		if (file_exists($systemFile)) {
			$systemXml = simplexml_load_file($systemFile);
			
			$nodes = $systemXml->xpath('sections/*/groups/*/fields/*/source_model');
			if ($nodes) {
				foreach ($nodes as $n) {
					$parents = $n->xpath('..');
					$parent = $parents[0];
					$domParent = dom_import_simplexml($parent);
					$path = explode('/', $domParent->getNodePath());
					$parent->config_path = "{$path[3]}/{$path[5]}/{$path[7]}";
				}
			}
			
			file_put_contents($systemFile, $systemXml->asXML());
		}
    
        return $this;
    }
    
    public function runCli($argv)
    {
        $this->_config = [];
        if (empty($argv[1]) || empty($argv[2])) {
            $this->_showUsage();
        }
        $this->_config['source_dir'] = $argv[1];
        $this->_config['target_dir'] = $argv[2];
        for ($i = 3, $l = sizeof($argv); $i < $l; $i++) {
            switch ($argv[$i]) {
                case '--debug':
                    $this->_config['debug'] = 1;
                    break;
            }
        }
        $this->convertAllModulesInDir();
    }
    
    protected function _showUsage()
    {
        echo <<<EOT

    USAGE: {$GLOBALS['argv'][0]} <sourceDir> <targetDir> [--debug]
        
        <sourceDir>  Root directory containing source modules folders in format:
                     "<Namespace>_<Vendor>". Module folders and files structure
                     should be the same as it would be in Magento 1 installation
                     
        <targetDir>  Root directory where "vendor" folder lives
        
        --debug      Show debug log information

EOT;
        die;
    }
    
    protected function _log($msg, $level = 'NOTICE')
    {
        if ($level === 'DEBUG' && empty($this->_config['debug'])) {
            return;
        }
        echo date('Y-m-d H:i:s') . ' ' . str_pad("[{$level}]", 10, ' ', STR_PAD_RIGHT) . " {$msg}\n";
    }
    
    protected function _runCmd($cmd)
    {
        $this->_log("RUN: {$cmd}", 'DEBUG');
        $result = `$cmd 2>&1`;
        if ($result) {
            $this->_log($result, 'DEBUG');
        }
        return $result;
    }
    
    protected function _copyTree($src, $dst)
    {
        @mkdir($dst, 0775, true);
        $this->_runCmd("cp -a {$src} {$dst}");
        return;
        
//        $dir = opendir($src); 
//        @mkdir($dst); 
//        while (false !== ($file = readdir($dir))) { 
//            if (($file !== '.') && ($file !== '..')) { 
//                $s = "{$src}/{$file}";
//                $d = "{$dst}/{$file}";
//                if (is_dir($s)) { 
//                    $this->_copyTree($s, $d); 
//                } else { 
//                    copy($s, $d); 
//                } 
//            } 
//        } 
//        closedir($dir); 
    }
    
    protected function _delTree($dir) 
    { 
        $files = array_diff(scandir($dir), ['.', '..']); 
        foreach ($files as $file) {
            $d = "{$dir}/{$file}";
            if (is_dir($d)) {
                $this->_delTree($d);
            } else {
                unlink($d); 
            }
        } 
        return rmdir($dir); 
    } 
}

if (!debug_backtrace() && PHP_SAPI === 'cli') {
    $converter = new ZoeyConvert();
    $converter->runCli($argv);
}